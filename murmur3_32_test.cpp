#include <systemc.h>
#include "tlm.h"
using namespace tlm;

#ifdef __RTL_SIMULATION__
#include "murmur3_32_rtl_wrapper.h"
#define murmur3_32 murmur3_32_rtl_wrapper
#else
#include "murmur3_32.h"
#endif

#include "tb_init.h"
#include "tb_driver.h"

int sc_main (int argc , char *argv[]) 
{

	// Remove simulation warnings
	sc_report_handler::set_actions("/IEEE_Std_1666/deprecated", SC_DO_NOTHING);
	sc_report_handler::set_actions( SC_ID_LOGIC_X_TO_BOOL_, SC_LOG);
	sc_report_handler::set_actions( SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_LOG);
	sc_report_handler::set_actions( SC_ID_OBJECT_EXISTS_, SC_LOG);
	// Test bench signals
	sc_signal<bool>	s_reset;
	sc_signal<bool>	s_enable;
	sc_signal<bool>	s_free;
	sc_signal<bool>	s_done;
	sc_signal<bool>	s_start;
	sc_signal<sc_uint<4> >	s_state;
	sc_signal<DT >	s_data_in;
	sc_signal<sc_uint<32> >	s_data_out;
    sc_signal<unsigned> s_seed;
    sc_signal<unsigned> s_keyL;

	sc_clock s_clk("s_clk",10,SC_NS);       // Create a 10ns period clock signal

	// Test bench modules
	tb_init	U_tb_init("U_tb_init");
	murmur3_f	U_dut("U_dut");
	tb_driver	U_tb_driver("U_tb_driver");
	 
	// Generate a reset & start signal to drive the sim
	U_tb_init.clk(s_clk);
	U_tb_init.reset(s_reset);
	U_tb_init.start(s_start);

	// Connect the DUT
	U_dut.clk(s_clk);
	U_dut.reset(s_reset);
	U_dut.enable_in(s_enable);
	U_dut.data_in(s_data_in);
	U_dut.free_out(s_free);
	U_dut.data_out(s_data_out);
	U_dut.done_out(s_done);
	U_dut.state_out(s_state);
	U_dut.seed(s_seed);
	U_dut.key_len(s_keyL);
	// Drive stimuli & Capture results
	U_tb_driver.clk(s_clk);
	U_tb_driver.reset(s_reset);
	U_tb_driver.start(s_start);
	U_tb_driver.free_in(s_free);
	U_tb_driver.data_in(s_data_out);
	U_tb_driver.done_in(s_done);
	U_tb_driver.state_in(s_state);
	U_tb_driver.enable_out(s_enable);
	U_tb_driver.data_out(s_data_in);
	U_tb_driver.seed(s_seed);
	U_tb_driver.key_l(s_keyL);
	// Sim 
	int end_time = 1000;
  
	cout << "INFO: Simulating " << endl;
	
	// start simulation 
	sc_start(end_time, SC_NS);

	// Print summary of results check
	if (U_tb_driver.retval != 0) {
		printf("Test failed  !!!\n"); 

	} else {
		printf("Test passed !\n");
  }
  return U_tb_driver.retval;

};

