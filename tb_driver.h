#ifndef TB_DRIVER_H
#define TB_DRIVER_H

#include "length_config.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <bitset>
using namespace std;

#include <systemc.h>

SC_MODULE(tb_driver)
{
	//Ports
  sc_in_clk    clk;
  sc_in <bool> reset;
  sc_in <bool> start;
  sc_in <bool> free_in;
  sc_in<sc_uint<32> > data_in;
  sc_in <bool> done_in;
  sc_in <sc_uint<4>> state_in;

  sc_out<DT> data_out;
  sc_out <bool> enable_out;
  sc_out<unsigned> key_l;
  sc_out<unsigned> seed;
	//Variables
  bool retval;
  sc_uint<32> res_sum[10];

	//Process Declaration
  void drive();
  void capture();

	//Constructor
  SC_CTOR(tb_driver)
  {
		// Input Driver process
    SC_CTHREAD(drive,clk.pos());
    reset_signal_is(reset,true);

		// Output Capture process
    SC_CTHREAD(capture,clk.pos());
    reset_signal_is(reset,true);
  }
};

#endif
