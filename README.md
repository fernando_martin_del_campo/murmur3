MURMUR 3 HLS MODULE
===

A SystemC HLS implementation of the murmur 3 hash function. Based on:
   
   https://github.com/PeterScott/murmur3.git

Only the MurmurHash3_x86_32 function is implemented, but the other two functions could easily be implemented using the existing code.

Be aware that the way you set the enable input could produce unexpected behaviours in the module.

Build
---

`vivado_hls -f run_script.tcl`

Open project
---

`vivado_hls -p proj_murmur3_32`

Simulation
---

Simultation only works with keys of 8 or less bytes, because of limitations of Vivado HLS (don't blame me).
You'll have to manually set the key and the length of the key in the tb_driver.cpp file.
