#ifndef MURMUR3_32_H
#define MURMUR3_32_H

// to enable fixed-poi
#define SC_INCLUDE_FX
#include <systemc.h>
#include <tlm.h>
#include "length_config.h"

using namespace tlm;

SC_MODULE(murmur3_f){
	//Ports
   sc_in<bool> clk; //clock input
   sc_in<bool> reset;
   sc_in<bool> enable_in;
   sc_in<DT> data_in;
   sc_in<unsigned> key_len;
   sc_in<unsigned> seed;

   sc_out<bool> free_out;
   sc_out<sc_uint<32> > data_out;
   sc_out<sc_uint<4> > state_out;
   sc_out<bool> done_out;
   sc_out<sc_biguint<32> > h1_out;
   sc_out<sc_biguint<32> > k1_out;

   //Process Declaration
   void murmur_thread();
	//Constructor
	SC_CTOR(murmur3_f)
   {
//#pragma HLS INTERFACE ap_ctrl_none port=clock
		//Process Registration
		SC_CTHREAD(murmur_thread,clk.pos());
		reset_signal_is(reset,true);
	}
};

#endif
