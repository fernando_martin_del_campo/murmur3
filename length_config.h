#ifndef LENGTH_CONFIG_H
#define LENGTH_CONFIG_H
#include <cstdint>
#include <systemc.h>
#include <string>

typedef uint64_t Datum;
typedef uint32_t Req_t;                  //Has to be larger or equal to Parse_t
typedef sc_uint<5> s_type;
typedef uint32_t Parse_t;

#define TRANS_1_IN_W                     64 //sizeof( Req_t ) * 8
#define TRANS_1_OUT_W                    32
#define REQ_OUT_W                        32
#define OUT_SIZE                         32
#define REQ_WORDS                         2
#define HASH_SIZE                        30

#define REQ_MODULES                       1
#define PARSE_MODULES                     1
#define TRANS_1_MODULES                   1
#define REG_MODULES                       1
#define READER_MODULES                    1
#define WRITER_MODULES                    1
#define NUM_OF_FLOWS                      1
#define LOG_TIMES                         1 //this should be = number of digits in NUM_OF_FLOWS (for examples 100 -> 3)
#define FLAG_1_MODS                       2

#define REG_PORT                          8

#define OP_L                              1
#define KEY_L                            24
#define KEY_L_B                         192
#define MAX_KEY                           1
#define WRITE_OP                          0
#define READ_OP                           1

#define MEM_SIZE                   67108864
#define REQ_MAX                         300
#define PARALLEL_MAP                      0

#define END_OF_REQUEST           1431655765 //(code for end of
#define TIMES_TO_REQ                      5
                                            // request 010101 ...)
#define DT sc_biguint<128>
#define DT2 sc_biguint<256>
//***********************************************************************************************//

#endif

