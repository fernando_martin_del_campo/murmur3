#include "murmur3_32.h"

void murmur3_f::murmur_thread()
{

   enum states { start, wait_enable, hash_for, hash_case, hash_epi_1, hash_epi_2, hash_epi_3, done, done_keep };
   //Initialization
   states state = start;
   bool enable_get = false;

   sc_biguint<128> data_get;

   int seed_get;
   sc_biguint<32> h1;

   sc_biguint<32> k1;
   sc_biguint<32> k1_tmp;
   sc_biguint<32> k1_tmp2;

   sc_biguint<128> data_tmp;
   sc_uint<32> key_len_get;

   int index_lo_3;
   int index_lo_2;
   int index_lo_1;
   int index_hi_1;

   unsigned c1 = 0xcc9e2d51;
   unsigned c2 = 0x1b873593;

   sc_uint<29> nblocks;

   unsigned i = 0;

   free_out->write(1);
   data_out->write(0);
   state_out->write(0);
   done_out->write(0);
   k1_out->write(0);
   h1_out->write(0);

   wait();
   seed_get = seed->read();

   while(1)
   {
      wait();
      state_out->write(state);

io_section:{
//#pragma HLS protocol fixed
      switch(state)
      {
         case start :
         {
            #ifndef __SYNTHESIS__
               cout << "\tstart" << endl;
            #endif
            h1 = seed_get;
            free_out->write(1);
            done_out->write(0);

            state = wait_enable;

            break;
         }
         case wait_enable :
         {
            #ifndef __SYNTHESIS__
               cout << "\twait_enable" << endl;
            #endif

            enable_get = enable_in->read();
            data_get   = data_in->read();
            key_len_get = key_len->read();
   			nblocks = key_len_get.range(31,2);


            if ( enable_get )
            {
               i = 0;
               data_tmp = 0;
               free_out->write(0);
               state = hash_for;
            }

            break;
         }
         case hash_for :
         {
            enable_get = false;
            if ( i < nblocks )
            {
                k1 = data_get >> i * 32;

            	k1 &= 0xFFFFFFFF;

            	k1_tmp = (k1 * c1)&0xFFFFFFFF;



            	k1 = k1_tmp << 15;

            	k1_tmp2 = (k1_tmp >> 17);
            	k1 = k1 | k1_tmp2;

            	wait();
            	k1 *= c2;
//#ifndef __SYNTHESIS__
//              cout << "k1 = " << std::hex <<  k1 << endl;
//#endif


            	k1_tmp = h1 ^ k1;

            	h1 = k1_tmp << 13;

            	k1_tmp2 = k1_tmp >> 19;
                h1 = h1 | k1_tmp2;

            	h1 = h1*5+0xe6546b64;

            	k1_out->write(k1);
            	h1_out->write(h1);
                wait();
                ++i;
            }
            else
            {

            	k1 = 0;
            	index_lo_1 = 128 - (key_len_get << 3);
            	index_hi_1 = index_lo_1 + ((key_len_get -1) << 3);

            	index_lo_2 = index_lo_1 + 8;

            	index_lo_3 = index_lo_1 + 16;

            	state = hash_case;
            }

            break;
         }
         case hash_case :
         {


            switch( key_len_get.range(1,0) )
            {
              	case 3:
              	{
              		data_tmp = data_get << index_lo_1;
              		data_tmp = data_tmp >> index_hi_1;
              		k1 ^= data_tmp << 16;
              		data_tmp = data_get << index_lo_2;
              		data_tmp = data_tmp >> index_hi_1;
              		k1 ^= data_tmp << 8;
              		data_tmp = data_get << index_lo_3;
              		data_tmp = data_tmp >> index_hi_1;
              		k1 ^= data_tmp;

              		state = hash_epi_1;
              		break;
              	}
                case 2:
                {
                	data_tmp = data_get << index_lo_1;
                	data_tmp = data_tmp >> index_hi_1;
                	k1 ^= data_tmp << 8;
                	data_tmp = data_get << index_lo_2;
                	data_tmp = data_tmp >> index_hi_1;
                	k1 ^= data_tmp ;

                	state = hash_epi_1;
                	break;
                }
                case 1:
                {
                	data_tmp = data_get << index_lo_1;
                	data_tmp = data_tmp >> index_hi_1;

                	k1 ^= data_tmp;

                	state = hash_epi_1;
                	break;
                }
                case 0:
                {
                    k1 = 0;
                    state = hash_epi_3;
                	break;
                }

            }

            k1_out->write(k1);
            break;
         }
         case hash_epi_1:
         {
            k1 *= c1;
            k1 = k1 << 15| k1 >> 17;
            k1_out->write(k1);
            state = hash_epi_2;
            break;
         }
         case hash_epi_2:
         {
            k1 *= c2;
            h1 ^= k1;
            k1_out->write(k1);
            h1_out->write(h1);
            state = hash_epi_3;
            break;
         }
         case hash_epi_3:
         {
        	h1 ^= key_len_get;

        	h1 ^= h1 >> 16;
        	h1 *= 0x85ebca6b;
            h1 ^= h1 >> 13;
            h1 *= 0xc2b2ae35;
            h1 ^= h1 >> 16;

            data_out->write(h1);
            done_out->write(1);

            state = done_keep;

            break;
         }
         case done_keep:
         {
            state = start;
            break;
         }
         default :
         {
            break;
         }
      }
   }}
}
