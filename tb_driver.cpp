#include "tb_driver.h"

void tb_driver::capture()
{
}

void tb_driver::drive()
{
   unsigned times = 0;
   enum states { check_if_free, wait_done, done };
   states state = check_if_free;
   bool done_get;
   sc_uint<32> data_get;
   sc_uint<4> state_get;
   seed->write(48);
	while (!start.read()) wait();

	ofstream result;
	result.open("result.dat");
	result << right << fixed << setbase(16) << setprecision(15);

   while(1)
   {
      switch(state)
      {
         case check_if_free :
            {

               if (free_in)
               {
                  enable_out->write(1);

                  key_l->write(8);
                  data_out->write(0x000000000000000032726569746d656d);
                  state = wait_done;
               }
               break;
            }
         case wait_done :
         {
            //std::cout << "Set enable" << std::endl;
            done_get = done_in->read();
            data_get = data_in->read();
            state_get = state_in->read();
            if (done_get)
            {
            	std::cout << "State = " << std::hex << state_get << std::endl;
               std::cout << "Output = " << std::hex << data_get << std::endl;
               state = done;
            }

            break;
         }
         case done :
         {
            retval=0;
            break;
         }
         default :
         {
            break;
         }
      }
      wait();
   }
}
