# Create a project
open_project -reset proj_murmur3_32

# Add design files
add_files murmur3_32.cpp -cflags "-D_GLIBCXX_HAVE_MBSTATE_T -std=c++0x"

# Add test bench & files
add_files -tb {murmur3_32_test.cpp tb_init.h tb_driver.h}
add_files -tb {tb_init.cpp tb_driver.cpp}
# Set the top-level function
set_top murmur3_f

# ########################################################
# Create a solution
open_solution -reset solution1
# Define technology and clock rate
set_part  {xcvu37p-fsvh2892-2-e-es1}
create_clock -period 3

# Source x_hls.tcl to determine which steps to execute
#	csynth_design
exit

